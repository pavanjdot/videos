import axios from 'axios'

const KEY = 'AIzaSyDKsOo2fGhChTjBah__ySUSG-AxlnQEOOs';

export default axios.create({
  baseURL: 'https://www.googleapis.com/youtube/v3',
  params:{
    part: 'snippet',
    maxResults: 5,
    key: KEY
  }
});