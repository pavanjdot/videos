import React from 'react'
import VideoList from './VideoList'
import SearchBar from './SearchBar'
import Youtube from '../Api/YoutubeApi'
import { async } from 'q';
import VideoDetail from './VideoDetail'


class App extends React.Component{
  state = {videos: [], onVideoSelect: null}

  componentDidMount(){
    this.onTermSubmit('divine');
  }

  onTermSubmit=async(term)=>{
    
    const response = await Youtube.get('/search', {
      params: {
        q: term
      }
    });

    
    this.setState(
      {
        videos: response.data.items,
        onVideoSelect: response.data.items[0]
      }
      
      )

  };

  onVideoSelect = video =>{
    this.setState({onVideoSelect: video})
  }
  
  render(){
    return(<div><SearchBar onFormSubmit={this.onTermSubmit}/>

    <div className="ui grid">
      <div className="ui row">
        <div className="eleven wide column">
        <VideoDetail video={this.state.onVideoSelect}/>
        </div>

        <div className="five wide column">
        <VideoList videos={this.state.videos}
                   onVideoSelect={this.onVideoSelect}
        />
        </div>

      </div>

    </div>
    
    
    

     
    </div>);

  }
}

export default App;